var images = ["background1.jpg", "background2.jpg", "background3.jpg", "background4.jpg"],
    base = "assets/images/",
    secs = 4;
images.forEach(function (img) {
    new Image().src = base + img;
});

function backgroundSequence() {
    window.clearTimeout();
    var index = 0;
    for (i = 0; i < images.length; i++) {
        setTimeout(function () {
            document.documentElement.style.background = "url(" + base + images[index] + ") no-repeat center center fixed";
            document.documentElement.style.backgroundSize = "cover";
            if ((index + 1) === images.length) {
                setTimeout(function () { backgroundSequence() }, (secs * 1000))
            } else {
                index++;
            }
        }, (secs * 1000) * i);
    }
}

backgroundSequence();
